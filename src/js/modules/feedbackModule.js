/**
 * Dit is de template van de feedback module gemaakt het het revealing module pattern
 * TODO omschrijving toevoegen en implementeren
 */
let feedbackModule = (function ($) {
    //-----------------
    //private module scope variables
    //-----------------
    var _configMap = {
        //TODO
    },
    _initModule,
    _toonSuccesBericht, _toonErrorBericht;
    
    _initModule = (container) => {
        //TODO
    };

    _toonSuccesBericht = (bericht) => {
        //TODO
        $("body").append(`Succes bericht! 👉 ${bericht} 🎉`);
    };

    _toonErrorBericht = (bericht) => {
        //TODO
        //console.log(`Error ${bericht} 😔`);
    };

    //-----------------
    //the (public) API
    //-----------------
    return {
        initModule: _initModule,
        toonSuccesBericht: _toonSuccesBericht,
        toonErrorBericht: _toonErrorBericht
    }
})(jQuery); //selfexecution anonymous function with a input parameter
