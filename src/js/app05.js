/*
 * JavaScript Classes
 */

//ES2015 notatie van de class function person(...)
class person {
    constructor(firstname, lastname, age, eyecolor) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.eyecolor = eyecolor;

        //setter function
        this.changeName = changeName;

        function changeName(name) {
            this.lastname = name;
        }
    }
}

let myMother = new person("Sally", "Rally", 48, "green");

console.log("Before");
console.log(myMother);
myMother.changeName("Doe");
console.log("After");
console.log(myMother);