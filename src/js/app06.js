//selfexecution function
(function () {

    //creeer nieuwe button (js first)
    let newButton = document.createElement("button");
    newButton.id = "myBtn";
    //newButton.style.backgroundColor = "#ff0000";
    newButton.innerHTML = "Click me!";
    
    //voeg de knop toe aan het body element
    let bodyElement = document.getElementsByTagName("body")[0];
    bodyElement.appendChild(newButton);

    //voeg een onclick event listner toe aan de knop
    document.getElementById("myBtn").onclick = function () {
        console.log("Yeah! I have been clicked");
    };

})();