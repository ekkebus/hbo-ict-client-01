/*
 * JavaScript Objects
 */

//object literal
let myFather = {
    firstname: "John",
    lastname: "Doe",
    age: 50,
    eyecolor: "blue"
};

//class functie met zelfde doel als de object literal
function person(firstname, lastname, age, eyecolor) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.age = age;
    this.eyecolor = eyecolor;
}

let myMother = new person("Sally", "Rally", 48, "green");
console.log(myFather);
console.log(myMother);