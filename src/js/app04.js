//zie ook: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain 
// en
//https://codepen.io/ekkebus/pen/bLEXmj?editors=1111

var obj1 = {
  a: 2,
  m: function() {
    return this.a + 1;
  }
};

var obj2 = Object.create(obj1);	//obj 2 erf van obj1
console.log("Obj2");
obj2.a = 4;   //set waarde
console.log(obj2.a);
console.log(obj2.m());
console.log(obj2.a);
console.log("Obj1");
console.log(obj1.a);
console.log(obj1.m());