/*
 *  JS functie als een variabele
 */

let variabele = function myFunction(name, job) {
    return `Welcome ${name} the ${job}`;    //Template literal: gebruik van bactics
}

console.log(variabele('Harry Potter', 'Wizard'));

/*
 *  Arrow functions, geeft hetzelfde resultaat
 */

let arrowFunctie = (name, job) => `Welcome ${name} the ${job}`;
console.log(arrowFunctie('Handige Harry', 'Builder'));
