/*
 *   let vs var
 * 
 * Verschil in scope.  
 *      var heeft scope tot het dichtsbijzijnde function block.
 *      let heeft scope tot het dichtsbijzijnde  enclosing block. 
 *      let kan niet opnieuw worden gedeclareerd in dezelfde scope, var wel.
 * 
 * Beiden zijn global als zij buiten enig block zijn.
 */
const constante = "toko";

function myFunction() {
    let lokaal = "loko toko";
    var lokaal_var = "loko toko 2";
    globaal = "global " + constante;
}

myFunction();
console.log(constante);
console.log(globaal);
console.log(lokaal);		//geeft foutmelding
console.log(lokaal_var);    //geeft foutmelding